
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from urlparse import urlparse, urlunparse, ParseResult
from SocketServer import ThreadingMixIn
from httplib import HTTPResponse
from tempfile import gettempdir
from os import path, listdir
from ssl import wrap_socket
from socket import socket
from select import select
from io import BytesIO
from re import compile
from sys import argv
import urllib2
import urllib
import time
import json
import zlib
import base64


def getDictIgnorCase(dic, key):
	if key in dic:
		return dic[key]
	for tkey in dic:
		if tkey.lower() == key.lower():
			return dic[tkey]
	return None

class Request(object):

	def __init__(self, 
		method,
		scheme,
		host,
		path,
		httpversion = 'HTTP/1.1',
		query = '',
		headers ={},
		content = ''
	):
		self.method = method
		self.scheme = scheme
		self.host = host
		self.path = path
		self.httpversion = httpversion
		self.query = query
		self.headers = headers
		self.content = content

		self.ishttps = self.scheme.lower() == 'https'
		thost = self.get_header('Host')
		if thost:
			(self.hostname, self.port) = (thost, 443 if self.ishttps else 80 ) if not ':' in thost else thost.split(':')
			self.port = int(self.port)

	def get_query(self):
		d = []
		a = self.query.split('&')
		for s in a:
			if '=' in s :
				k,v = map(urllib.unquote, s.split('=',1))
				d.append((k,v))
		return d

	def get_header(self,key):
		return getDictIgnorCase(self.headers, key)

	def fpath(self):
		return self.path if len(self.query) == 0 else '%s?%s'%(self.path,self.query)

	def url(self):
		return '%s://%s%s' % (self.scheme, self.host, self.fpath())

	@staticmethod
	def load_raw(data, ishttps=False):
		with BytesIO(data) as stream:
			method, pathq, httpv = stream.readline().strip().split(' ')
			path = pathq
			query = ''
			if '?' in pathq : path, query = pathq.split('?',1)
			headers = {}
			while True:
				header = stream.readline().strip()
				if len(header) == 0 : break;
				key,valu = header.split(':',1)
				headers[key.strip()] = valu.strip()
				if key.strip().lower() == 'host' and valu.strip().endswith(':443'):
					ishttps = True

			content = ''
			if method.lower() == 'post':
				#content = stream.read(int(getDictIgnorCase(headers, 'Content-Length')))
				content = stream.read()
			scheme = 'https' if ishttps else 'http'
			req = Request(method,scheme,getDictIgnorCase(headers, 'Host'),path,httpversion=httpv, query=query, headers=headers, content=content )
		return req

	def dump_raw(self):
		raw = '%s %s %s\r\n' % (self.method, self.fpath(), self.httpversion)
		for key in self.headers:
			raw += '%s: %s\r\n' % (key, self.headers[key])
		raw += '\r\n'
		if len(self.content) > 0 :
			raw += self.content

		return raw

	@staticmethod
	def load_json(data):
		data = json.loads(data, encoding='latin1')
		req = Request('','','','')
		for key in data:
			req.__setattr__(key, data[key])
		if len(req.content)>0:
			req.content = req.content.decode('base64')
		return req

	def dump_json(self):
		if len(self.content)>0:
			self.content = base64.b64encode(self.content)
		req = json.dumps(self.__dict__, encoding='latin1')
		if len(self.content)>0:
			self.content = self.content.decode('base64')
		return req

class MyIo(BytesIO):
	def makefile(self,a,b):
		return self
'''
	def read(self, rlen = None):
		if rlen:
			data = super(MyIo, self).read(rlen)
		else :
			data = super(MyIo, self).read()
		if len(data) == 0 : raise Exception('unable read empty data')
		return data

	def readline(self, rlen = None):
		if rlen:
			data = super(MyIo, self).readline(rlen)
		else :
			data = super(MyIo, self).readline()
		if len(data) == 0 : raise Exception('unable read empty data')
		return data
'''
class Response(object):

	def __init__(self, 
		httpversion,
		status,
		reason,
		headers ={},
		content = ''
	):
		self.httpversion = httpversion
		self.status = status
		self.reason = reason
		self.headers = headers
		self.content = content

	def get_header(self,key):
		return getDictIgnorCase(self.headers, key)

	def get_content(self):
		if str(self.get_header('content-encoding')).lower() == 'gzip':
			return zlib.decompress(self.content, 16+zlib.MAX_WBITS)
		return self.content

	@staticmethod
	def load_resp(rsp):
		rsp.begin()
		# Get rid of the pesky header
		del rsp.msg['Transfer-Encoding']
		vs = 'HTTP/%s' % '.'.join(str(rsp.version))
		rsp = Response(vs, rsp.status, rsp.reason, dict(rsp.msg), rsp.read())
		return rsp

	@staticmethod
	def load_raw(data):
		httpversion = ''
		status = ''
		reason = ''
		headers = {}
		content = ''

		(hdata,content) = data.split('\r\n\r\n',1)
		hdata += '\r\n'
		(fline,pheaders) = hdata.split('\r\n',1)
		pheaders = pheaders.split('\r\n')
		(httpversion,status) = fline.split(' ',1)
		if ' ' in status: (status,reason) = status.split(' ',1)
		for ph in pheaders:
			if len(ph.strip()) == 0 : continue
			(k,v) = ph.split(':',1)
			headers[k.strip()] = v.strip()
		rsp = Response(httpversion, status, reason, headers, content)
		return rsp
#		rsp = None
#		with MyIo(data) as mio:
#			trsp = HTTPResponse(mio)
#			rsp = Response.load_resp(trsp)
#			trsp.close()
#		return rsp

	def dump_raw(self):
		raw = '%s %s %s\r\n' % (self.httpversion, self.status, self.reason)
		# not sure about this
		if 'Transfer-Encoding' in self.headers: del self.headers['Transfer-Encoding']
		if 'transfer-encoding' in self.headers: del self.headers['transfer-encoding']
		for key in self.headers:
			raw += '%s: %s\r\n' % (key, self.headers[key])
		raw += '\r\n'
		raw = str(raw)
		raw += self.content
		return raw

	def dump_json(self):
		if len(self.content)>0:
			self.content = base64.b64encode(self.content)
		resp = json.dumps(self.__dict__, encoding='latin1')
		if len(self.content)>0:
			self.content = self.content.decode('base64')
		return resp

	@staticmethod
	def load_json(data):
		data = json.loads(data, encoding='latin1')
		resp = Response('','','','','')
		for key in data:
			resp.__setattr__(key, data[key])
		if len(resp.content)>0:
			resp.content = resp.content.decode('base64')
		return resp

	@staticmethod
	def is_integrity(data):
		try:
			with MyIo(data) as mio:
				trsp = HTTPResponse(mio)
				trsp.begin()
				#print 'len:%s' % trsp.length
				trsp.read()
				trsp.close()
			return True
		except Exception,e:
			#print e
			pass
		return False
def play_req(req,timeout = None):
	resp = None
	sock = socket()
	sock.connect((req.hostname, req.port))
	if req.ishttps:
		sock = wrap_socket(sock)
	sock.send(req.dump_raw());
	if timeout == None:
		trsp = HTTPResponse(sock)
		resp = Response.load_resp(trsp)
		trsp.close()
	else :
		data = ''
		while 1:
			data += select([sock], [], [],timeout)[0][0].recv(10240)
			print len(data)
			if Response.is_integrity(data): break
		print data
		resp = Response.load_raw(data)
	sock.close()
	return resp

def play_urllib_req(req,timeout = None):
	url = req.url()
	data = req.content
	headers = req.headers.copy()
	if data:
		requrl = urllib2.Request(url, data, headers=headers) 
	else:
		requrl = urllib2.Request(url, headers=headers) 
	resp = urllib2.urlopen(requrl)
	return resp

class HTTPFlow(object):
	def __init__(self, req):
		self.req = req
		self.rsp = None

a = 'R0VUIC9nMy9wcm90b2NvbC9HZXRTZXJ2ZXJUaW1lLmpzcD9mcm9tPWFuZHJvaWQgSFRUUC8xLjENClVzZXItQWdlbnQ6IERhbHZpay8xLjYuMCAoTGludXg7IFU7IEFuZHJvaWQgNC40LjQ7IFNIVi1FMjEwTCBCdWlsZC9LVFU4NFApDQpIb3N0OiBnb3JlYWxyYS5zYnMuY28ua3INCkNvbm5lY3Rpb246IEtlZXAtQWxpdmUNCkFjY2VwdC1FbmNvZGluZzogZ3ppcA0KDQo='
#b = Response.load_raw(a.decode('base64')).dump_json()
#c = Response.load_json(b)
b = 'SFRUUC8xLjEgMjAwIE9LDQpEYXRlOiBUdWUsIDAyIEZlYiAyMDE2IDE3OjEzOjQ0IEdNVA0KU2VydmVyOiBBcGFjaGUNClNldC1Db29raWU6IFdNT05JRD1qUEt1alZ1ZXNGRTsgZXhwaXJlcz1XZWRuZXNkYXksIDAxLUZlYi0yMDE3IDE3OjEzOjQ0IEdNVDsgcGF0aD0vDQpTZXQtQ29va2llOiBKU0VTU0lPTklEPTF6SkpXd2pMbkhnZ3NHeVRuUVBGUkMyZzBKY1ZiTndyME13SHZCbnY3Nmx0bjFManc0RHMhLTI1MzgxMjUzNjsgcGF0aD0vDQpDYWNoZS1Db250cm9sOiBtYXgtYWdlPTg2NDAwLCBuby1jYWNoZQ0KRXhwaXJlczogV2VkLCAwMyBGZWIgMjAxNiAxNzoxMzo0NCBHTVQNClAzUDogQ1A9J7Cjt6u55sSnseLIoycNCktlZXAtQWxpdmU6IHRpbWVvdXQ9NSwgbWF4PTEwMA0KQ29ubmVjdGlvbjogS2VlcC1BbGl2ZQ0KQ29udGVudC1UeXBlOiB0ZXh0L3htbDtjaGFyc2V0PVVURi04DQpDb250ZW50LUxhbmd1YWdlOiBrbw0KQ29udGVudC1MZW5ndGg6IDE4NQ0KDQo8P3htbCB2ZXJzaW9uPSIxLjAiIGVuY29kaW5nPSJVVEYtOCI/Pg0KPFRJTUU+DQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KPCEtLTM1NDA3LS0+CjwhLS1EYXRhQmFzZSAtLT4KPFdFQlNFUlZFUj48IVtDREFUQVsiMjAxNjAyMDMwMjEzNDUiXV0+PC9XRUJTRVJWRVI+Cg0KPC9USU1FPg=='

Request.load_json(Request.load_raw(a.decode('base64')).dump_json())
Response.load_json(Response.load_raw(b.decode('base64')).dump_json())
